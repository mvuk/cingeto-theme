<?php
/**
 * Template Name: whoweare-team-nicolas
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg abs-team hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu" id="msmMenu" onclick="showMsm()">
			<span>Who we are</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/who-we-are/mission/" class="msm-a">
				Mission
			</a>
			<a href="/who-we-are/global/" class="msm-a">
				Global
			</a>
			<a href="/who-we-are/values/" class="msm-a">
				Values
			</a>
			<a href="/who-we-are/team" class="msm-a active">
				Team
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Team</h3>
		</div>
		<div class="team-profile">
			<img src="/wp-content/uploads/2018/04/nicolas-hine-1.jpg" alt="Nicolas Hine">
			<div class="details">
				<!-- <p>languages? education? direct contact information?</p> -->
			</div>
			<div class="back">
				<a href="/contact/">Get in touch</a>
				<a href="/who-we-are/team">Return to full team</a>
			</div>
		</div>
 	</div>
</div>

<div class="fullHW full-image profile">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content">
						<h1 class="min-title"><a href="/who-we-are/" class="linline-h1-link">Who we are</a>&gt; <a href="/who-we-are/team" class="linline-h1-link">Team</a></h1>
						<h1>Nicolas Hine</h1>
						<h2>Chief Financial Officer</h2>

						<!-- mobile -->
						<div class="showMobile profile-mobile">
							<img src="/wp-content/uploads/2018/04/nicolas-hine-1.jpg" alt="Nicolas Hine">
						</div>
						<!--  -->
					</div>
				</div>
				<div class="col-md-12">
					<p>Nicolas started his career in Public Accounting at KPMG and PwC in Paris. Before co-founding Cingeto, Nicolas moved his way up, holding progressively more senior operational and corporate finance roles for multinational groups in South Korea, France, and the US.</p>
					<p>Nicolas is business-oriented and naturally curious with a strong background in Finance on both the controlling and the auditing sides.  Finance, internal controls, processes, risk assessment, reporting, and analysis are his forte but he also always had a strong interest in cutting-edge technologies and innovation. He developed a natural ability for business strategy as a result.</p>
					<p>Nicolas holds a degree in East Asian Literature, Languages, and Civilizations from Paris VII and as well as a Master of Business from ISC Paris and speaks fluent French, English, and Korean.</p>
					<p>At Cingeto, Nicolas is able to utilize all of his past experiences and what he learned throughout the years.  He enjoys being a partner to organizations and a part of their adventures and challenges.</p>
					<div class="back showMobile">
						<a href="/contact/" class="c-btn">Get in touch</a>
						<a href="/who-we-are/team" class="c-btn">Return to full team</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
