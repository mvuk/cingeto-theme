<?php
/**
 * Template Name: industries-innovative
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu threeMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>Industries</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/industries/aerospace/" class="msm-a">
				Aerospace
			</a>
			<a href="/industries/space/" class="msm-a">
				Space
			</a>
			<a href="/industries/innovative-technologies/" class="msm-a active">
				Innovative Technologies
			</a>
		</div>
	</div>
	<!-- end mobile menu -->

	<div class="sub-links">
		<div class="sub-title">
			<h3>Industries</h3>
		</div>
		<div class="sub-link">
			<a href="/industries/aerospace/">
				Aerospace
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/space/">
				Space
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/innovative-technologies/" class="active">
				Innovative Technologies
			</a>
		</div>
 	</div>
</div>

<div class="fullHW innovative full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content industry">
						<h1 class="min-title"><a href="/industries/" class="linline-h1-link">Industries</a>&gt; Innovative Technologies</h1>
						<h2>We participate in projects that make the world a better place</h2>
						<p>We have direct experience working for both commercial space and government programs in these areas:</p>
						<ul>
							<li>Transportation Technology</li>
							<li>Machine Learning</li>
							<li>AR</li>
							<li>IoT</li>
							<li>Energy Technology</li>
						</ul>
						<div class="outro-link">
							<p>If you are not on the list, but still a member of the innovative technology community, let us start a conversation. We always appreciate meeting new people.</p>
							<a href="/our-clients/eyelights/" class="c-btn">Read our innovative tech case study</a>
							<a href="/contact/" class="c-btn">Send us a message</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
