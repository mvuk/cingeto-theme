<?php
/**
 * Template Name: ourclients
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu threeMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>Our Clients</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/our-clients/airbus-oneweb-satellites/" class="msm-a">
				Airbus OneWeb Satellites
			</a>
			<a href="/our-clients/aim-aerospace/" class="msm-a">
				AIM Aerospace
			</a>
			<a href="/our-clients/eyelights/" class="msm-a">
				EyeLights
			</a>
		</div>
	</div>
	<!-- end mobile menu -->

 	<div class="sub-links">
		<div class="sub-title">
			<h3>Clients</h3>
		</div>
		<div class="sub-link">
			<a href="/our-clients/airbus-oneweb-satellites/">
				Airbus OneWeb Satellites
			</a>
		</div>
		<div class="sub-link">
			<a href="/our-clients/aim-aerospace/">
				AIM Aerospace
			</a>
		</div>
		<div class="sub-link">
			<a href="/our-clients/eyelights/">
				EyeLights
			</a>
		</div>
		<!-- <div class="sub-link">
			<a href="/our-clients/candidates/">
				Candidates
			</a>
		</div> -->
 	</div>
</div>

<div class="fullHW ourclientssay full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="full-height-center">
				<div class="content">
					<h1>Our Clients</h1>
					<h2>Read testimonials from our clients.</h2>
					<!-- <p>We are the north star on our work.</p> -->
					<p>We work hand in hand with companies such as Airbus OneWeb Satellites, AIM Aerospace and EyeLights to create synergy between their talent and business strategy.</p>
				</div>
			</div>
			<!--  -->
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
