<?php
/**
 * Template Name: whatwedo-organizationaldevelopment
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu fiveMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>What we do</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/what-we-do/organizational-development/" class="msm-a active">
				Organizational Development
			</a>
			<a href="/what-we-do/recruitment-talent/" class="msm-a">
				Recruitment & Talent
			</a>
			<a href="/what-we-do/global-growth/" class="msm-a">
				Global Growth
			</a>
			<a href="/what-we-do/coaching/" class="msm-a">
				Coaching
			</a>
			<a href="/what-we-do/ma-and-cultural-integration/" class="msm-a">
				M&A and Cultural Integration
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>What we do</h3>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/organizational-development/" class="active">
				Organizational Development
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/recruitment-talent/">
				Recruitment & Talent
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/global-growth/">
				Global Growth
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/coaching/">
				Coaching
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/ma-and-cultural-integration/">
				M&A and Cultural Integration
			</a>
		</div>
 	</div>
</div>

<div class="fullHW orgdev full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content">
						<h1 class="min-title"><a href="/what-we-do/" class="linline-h1-link">What we do</a>&gt; Organizational Development</h1>
						<h2>Our approach to organizational development is deeply rooted in anthropology and borrows from the humanistic perspective.</h2>
						<p>Cingeto provides you avenues for change focusing on improving organization, culture, knowledge, exchange, and collaboration.</p>
						<p>Organizational development offerings:</p>
						<div class="row">
							<div class="col-md-6">
								<ul class="indent-ul">
									<li>Organizational Readiness Assessment</li>
									<li>Talent Gap Analysis</li>
									<li>360 Feedback</li>
									<li>New Leader Assimilation</li>
								</ul>
							</div>
							<div class="col-md-6">
								<ul class="indent-ul">
									<li>Development Planning</li>
									<li>Succession Planning</li>
									<li>Retention Strategy</li>
									<li>Internal Talent Acquisition Development</li>
								</ul>
							</div>
						</div>
						<ul>
						</ul>
						<div class="outro-link">
							<a href="/industries/" class="c-btn">Explore industries</a>
							<a href="/contact/" class="c-btn">Send us a message</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
