<?php
/**
 * Template Name: whoweare-team-juliette
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg abs-team hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu" id="msmMenu" onclick="showMsm()">
			<span>Who we are</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/who-we-are/mission/" class="msm-a">
				Mission
			</a>
			<a href="/who-we-are/global/" class="msm-a active">
				Global
			</a>
			<a href="/who-we-are/values/" class="msm-a">
				Values
			</a>
			<a href="/who-we-are/team" class="msm-a active">
				Team
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Team</h3>
		</div>
		<div class="team-profile">
			<img src="/wp-content/uploads/2018/04/juliette-neu-1.jpg" alt="Juliette Neu">
			<div class="details">
				<!-- <p>languages? education? direct contact information?</p> -->
			</div>
			<div class="back">
				<a href="/contact/">Get in touch</a>
				<a href="/who-we-are/team">Return to full team</a>
			</div>
		</div>
 	</div>
</div>

<div class="fullHW full-image profile">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content">
						<h1 class="min-title"><a href="/who-we-are/" class="linline-h1-link">Who we are</a>&gt; <a href="/who-we-are/team" class="linline-h1-link">Team</a></h1>
						<h1>Juliette Neu</h1>
						<h2>Chief Executive Officer</h2>

						<!-- mobile -->
						<div class="showMobile profile-mobile">
							<img src="/wp-content/uploads/2018/04/juliette-neu-1.jpg" alt="Juliette Neu">
						</div>
						<!--  -->
					</div>
				</div>
				<div class="col-md-12">
					<p>Juliette Neu started her career in talent strategy in 1998. She was based in South Korea and consulted across East Asia for various industries with a focus on organizational assessments, team integration, and cultural alignment. Juliette has deep experience advising the Advanced Manufacturing sector and successfully led recruiting ramp-ups for major international projects in that sphere. She co-founded Cingeto in 2017.</p>
					<p>Prior to launching Cingeto, Juliette served as Partner for the Advanced Manufacturing and Innovative Technology sectors at a large firm with a focus on global accounts covering the Americas, Europe and Asia. She lived and worked in Europe, West Africa, East Asia, and North America and speaks four languages (English, French, Korean, and Spanish). She holds a Master of Arts in Applied Anthropology from American University and began doctoral research in 2012 in Anthropology at the University of Pittsburgh. As a multilingual and multicultural anthropologist with years of experience working on a variety of talent strategy projects, Juliette brings uncommon skills and expertise to consulting. She is a relationship builder and a trusted advisor to her clients and their teams, with a successful track record of creating alignment between them. It is her hope that by successfully partnering with clients, Cingeto can contribute to the advancement of technology that ultimately benefits us all.</p>
					<div class="back showMobile">
						<a href="/contact/" class="c-btn">Get in touch</a>
						<a href="/who-we-are/team" class="c-btn">Return to full team</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
