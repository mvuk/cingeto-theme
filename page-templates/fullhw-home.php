<?php
/**
 * Template Name: home
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-home-menu hideDuringMenu" id="abs-menu" onclick="fadeSelfAway()">
	<div class="abs-h-side">
		<div class="abs-h-item">
			<a class="abs-h-a" href="/who-we-are/">
				<div class="label">
					Who we are
				</div>
			</a>
		</div>
		<div class="abs-h-item">
			<a class="abs-h-a hideMobile" href="/industries/">
				<div class="label">
					Industries
				</div>
			</a>
			<a class="abs-h-a hideDesktop" href="/what-we-do/">
				<div class="label">
					What we do
				</div>
			</a>
		</div>
	</div>
	<div class="abs-h-side">
		<div class="abs-h-item">
			<a class="abs-h-a hideMobile" href="/what-we-do/">
				<div class="label">
					What we do
				</div>
			</a>
			<a class="abs-h-a hideDesktop" href="/industries/">
				<div class="label">
					Industries
				</div>
			</a>
		</div>
		<div class="abs-h-item">
			<a class="abs-h-a" href="/our-clients/">
				<div class="label">
					Our clients
				</div>
			</a>
		</div>
	</div>
</div>

<div class="fullHW home">
	<!--  -->
	<div class="homeArea hideDuringMenu" id="abs-container">
		<!--  -->
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="headline">
					<h1>Profoundly Human</h1>
					<p class="hideMobile">Transcending organizational boundaries one human at a time.</p>
					<p class="hideMobile">We apply an anthropological approach to your talent needs in order to maximize your corporate success.</p>
					<div class="hideMobile homeList">
						<ul>
							<li>Organizational Development</li>
							<li>Recruitment and Talent (Executive Search & Team Ramp-ups)</li>
							<li>Global Growth</li>
							<li>Coaching</li>
							<li>M&A and Cultural Integration</li>
						</ul>
					</div>
					<!-- <p>Our approach enables us to assess, align, and grow successful teams for you.</p> -->
					</div>
				</div>
				<!-- <div class="col-md-7">
					<div class="compass-container">
						<div class="compass">
							<div class="compass-element one">
								<a href="/what-we-do/" class="compass-element-a">What we do</a>
							</div>
							<div class="compass-element two">
								<a href="/who-we-are/" class="compass-element-a">Who we are</a>
							</div>
							<div class="compass-element three">
								<a href="/our-clients/" class="compass-element-a">Our clients</a>
							</div>
						</div>
					</div>
					<img src="/wp-content/uploads/2018/04/cingeto-compass-rose-concept.jpg" alt="">
				</div> -->
			</div>
		</div>
		<!--  -->
	</div>
	<!--  -->
</div>

<div class="homeVideo">
	<div class="videoDiv">
		<video autoplay loop class="videoElement" poster="/wp-content/uploads/2018/05/video-poster-kite.jpg" id="video">
			<source src="/wp-content/uploads/2018/05/cingeto-home-kite.mp4" type="video/mp4">
			<source src="/wp-content/uploads/2018/05/cingeto-ocean-kite-update.mp4" type="video/mp4">
			<source src="/wp-content/uploads/2018/04/cingeto-home-kite.webm" type="video/webm">
		</video>
	</div>
</div>

<script type="text/javascript">
	document.getElementById('video').play();
</script>

<!-- PAGE CONTENT END -->
<?php
get_footer();
