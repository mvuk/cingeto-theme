<?php
/**
 * Template Name: industries-space
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu threeMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>Industries</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/industries/aerospace/" class="msm-a">
				Aerospace
			</a>
			<a href="/industries/space/" class="msm-a active">
				Space
			</a>
			<a href="/industries/innovative-technologies/" class="msm-a">
				Innovative Technologies
			</a>
		</div>
	</div>
	<!-- end mobile menu -->

	<div class="sub-links">
		<div class="sub-title">
			<h3>Industries</h3>
		</div>
		<div class="sub-link">
			<a href="/industries/aerospace/">
				Aerospace
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/space/" class="active">
				Space
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/innovative-technologies/">
				Innovative Technologies
			</a>
		</div>
 	</div>
</div>

<div class="fullHW space full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content industry">
						<h1 class="min-title"><a href="/industries/" class="linline-h1-link">Industries</a>&gt; Space</h1>
						<h2>We are all about exploration and discovery</h2>
						<p>We have direct experience working for both commercial space and government programs in these areas:</p>
						<ul class="indent-ul">
							<li>Space Systems and Spacecraft</li>
							<li>Space-based Communication Technologies</li>
							<li>Space Automation and Robotics</li>
							<li>Space Logistics and Supply Chain</li>
							<li>Space Operations and Support</li>
						</ul>
						<div class="outro-link">
							<p>If you are not on the list, but still a member of the space community, let us start a conversation. We always appreciate meeting new people.</p>
							<a href="/our-clients/airbus-oneweb-satellites//" class="c-btn">Read our space case study</a>
							<a href="/contact/" class="c-btn">Send us a message</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
