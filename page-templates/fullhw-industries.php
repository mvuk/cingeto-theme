<?php
/**
 * Template Name: industries
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop in" id="mobileSubMenu">
		<div class="msmMenu threeMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>Industries</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/industries/aerospace/" class="msm-a">
				Aerospace
			</a>
			<a href="/industries/space/" class="msm-a">
				Space
			</a>
			<a href="/industries/innovative-technologies/" class="msm-a">
				Innovative Technologies
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Industries</h3>
		</div>
		<div class="sub-link">
			<a href="/industries/aerospace/">
				Aerospace
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/space/">
				Space
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/innovative-technologies/">
				Innovative Technologies
			</a>
		</div>
 	</div>
</div>

<div class="fullHW industries full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="full-height-center">
				<div class="content">
					<h1>Industries</h1>
					<h2>At Cingeto, we are visionaries inspired by innovators and groundbreakers.</h2>
					<!--  -->
					<p>Our focus has allowed us to build a strong international network of scientists, researchers, engineers, business operations leaders, and executives.</p>
					<!-- <p>We work hand in hand with companies such as Air Liquide, Aim Aerospace, and Airbus OneWeb Satellites to create synergy between their talent and business strategy.</p> -->
				</div>
			</div>
			<!--  -->
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
