<?php
/**
 * Template Name: industries-aerospace
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu threeMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>Industries</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/industries/aerospace/" class="msm-a active">
				Aerospace
			</a>
			<a href="/industries/space/" class="msm-a">
				Space
			</a>
			<a href="/industries/innovative-technologies/" class="msm-a">
				Innovative Technologies
			</a>
		</div>
	</div>
	<!-- end mobile menu -->

	<div class="sub-links">
		<div class="sub-title">
			<h3>Industries</h3>
		</div>
		<div class="sub-link">
			<a href="/industries/aerospace/" class="active">
				Aerospace
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/space/">
				Space
			</a>
		</div>
		<div class="sub-link">
			<a href="/industries/innovative-technologies/">
				Innovative Technologies
			</a>
		</div>
 	</div>
</div>

<div class="fullHW aerospace full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content industry">
						<h1 class="min-title"><a href="/industries/" class="linline-h1-link">Industries</a>&gt; Aerospace</h1>
						<h2>Keep fueling one of the oldest human dreams</h2>
						<p>We have direct experience working with OEMs, Tier 1 and Tier 2 suppliers, as well as MROs in these areas:</p>
						<ul>
							<li>Structures</li>
							<li>Interiors</li>
							<li>Engines</li>
							<li>Avionics</li>
							<li>Rotorcrafts</li>
						</ul>
						<div class="outro-link">
							<p>If you are not on the list, but still a member of the aerospace community, let us start a conversation. We always appreciate meeting new people.</p>
							<a href="/our-clients/aim-aerospace/" class="c-btn">Read our aerospace case study</a>
							<a href="/contact/" class="c-btn">Send us a message</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
