<?php
/**
 * Template Name: ourclients-oneweb
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu threeMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>Our Clients</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/our-clients/airbus-oneweb-satellites/" class="msm-a active">
				Airbus OneWeb Satellites
			</a>
			<a href="/our-clients/aim-aerospace/" class="msm-a">
				AIM Aerospace
			</a>
			<a href="/our-clients/eyelights/" class="msm-a">
				EyeLights
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Clients</h3>
		</div>
		<div class="sub-link">
			<a href="/our-clients/airbus-oneweb-satellites/" class="active">
				Airbus OneWeb Satellites
			</a>
		</div>
		<div class="sub-link">
			<a href="/our-clients/aim-aerospace/">
				AIM Aerospace
			</a>
		</div>
		<div class="sub-link">
			<a href="/our-clients/eyelights/">
				EyeLights
			</a>
		</div>
		<!-- <div class="sub-link">
			<a href="/our-clients/candidates/">
				Candidates
			</a>
		</div> -->
 	</div>
</div>

<div class="fullHW client-page oneweb">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row client-intro">
				<div class="col-md-12">
					<!-- <img src="/wp-content/uploads/2018/04/oneweb-satelites.jpg" alt="OneWeb" class="client-feature-img"> -->
					<!-- <img src="/wp-content/uploads/2018/04/one-web-production-line-inauguration-toulouse.jpg" alt="OneWeb" class="client-feature-img"> -->
				</div>
				<div class="col-md-12 title">
					<h1 class="min-title"><a href="/our-clients/" class="linline-h1-link">Our clients</a>&gt; Airbus Oneweb Satellites</h1>
					<h1>Airbus OneWeb Satellites</h1>
				</div>
			</div>
			<div class="row client-details">
				<div class="col-md-12">
					<div class="section quote">
						<p class="text">"Finding the right talent is a very tedious task. A proactive and strategic approach is much needed and job postings aren’t quite enough.</p>
						<p class="text">You need a partner you can build a long term relationship with and who can guide the process, and you need to be able to trust that they will represent your company and its culture in recruiting your future team members.  Cingeto is doing just that. They are successful at leading fast-paced talent ramp-ups and sourcing candidates both in the US and in Europe. The quality and caliber of the candidates and services are outstanding and their team trustworthy."</p>
						<div class="attribute">
							<p>- Kai Schmidt, Director of HR at Airbus OneWeb Satellites</p>
						</div>
					</div>
					<div class="outro-link">
						<a href="/contact/" class="c-btn">Send us a message</a>
					</div>

				</div>
			</div>
			<!--  -->
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
