<?php
/**
 * Template Name: ourclients-eyelights
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu threeMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>Our Clients</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/our-clients/airbus-oneweb-satellites/" class="msm-a">
				Airbus OneWeb Satellites
			</a>
			<a href="/our-clients/aim-aerospace/" class="msm-a">
				AIM Aerospace
			</a>
			<a href="/our-clients/eyelights/" class="msm-a active">
				EyeLights
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Clients</h3>
		</div>
		<div class="sub-link">
			<a href="/our-clients/airbus-oneweb-satellites/">
				Airbus OneWeb Satellites
			</a>
		</div>
		<div class="sub-link">
			<a href="/our-clients/aim-aerospace/">
				AIM Aerospace
			</a>
		</div>
		<div class="sub-link">
			<a href="/our-clients/eyelights/" class="active">
				EyeLights
			</a>
		</div>
		<!-- <div class="sub-link">
			<a href="/our-clients/candidates/">
				Candidates
			</a>
		</div> -->
 	</div>
</div>

<div class="fullHW client-page eyelights">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row client-intro">
				<div class="col-md-12">
					<!-- <img src="/wp-content/uploads/2018/04/eyelights-night.jpg" alt="Eyelights" class="client-feature-img"> -->
					<!-- <img src="/wp-content/uploads/2018/04/eyelights-motorcycle-demo.jpg" alt="Eyelights" class="client-feature-img"> -->
				</div>
				<div class="col-md-12 title">
					<h1 class="min-title"><a href="/our-clients/" class="linline-h1-link">Our clients</a>&gt; Eyelights</h1>
					<h1>EyeLights</h1>
				</div>
			</div>
			<div class="row client-details">
				<div class="col-md-12">
					<div class="section quote">
						<p class="text">"Cingeto was referred to us by one of their current clients.  We are a rapidly growing French startup specialized in IoT/AR.  Cingeto has become a reliable advisor whom we trust. They are competent global talent scouts."</p>
						<div class="attribute">
							<p>- Thomas de Saintignon, Co-Founder and CTO at EyeLights</p>
						</div>
					</div>
					<div class="outro-link">
						<a href="/contact/" class="c-btn">Send us a message</a>
					</div>
				</div>
			</div>
			<!--  -->
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
