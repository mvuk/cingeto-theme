<?php
/**
 * Template Name: whoweare-team
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu" id="msmMenu" onclick="showMsm()">
			<span>Who we are</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/who-we-are/mission/" class="msm-a">
				Mission
			</a>
			<a href="/who-we-are/global/" class="msm-a">
				Global
			</a>
			<a href="/who-we-are/values/" class="msm-a">
				Values
			</a>
			<a href="/who-we-are/team" class="msm-a active">
				Team
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Who we are</h3>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/mission/">
				Mission
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/global/">
				Global
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/values/">
				Values
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/team" class="active">
				Team
			</a>
		</div>
 	</div>
</div>

<div class="fullHW team full-image" id="fullImg">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content">
						<h1 class="min-title"><a href="/who-we-are/" class="linline-h1-link">Who we are</a>&gt; Team</h1>
						<h2>We apply what we advocate to ourselves</h2>
						<p>We believe diversity goes beyond gender, age or physical appearance.</p>
						<p>Our company respects all humans and their unique experiences.</p>
						<p>Cingeto is a powerful team who values diversity as not only a strength but as a launch pad to success.</p>
					</div>
				</div>
				<div class="col-md-6">
					<a href="/who-we-are/team/juliette-neu/">
						<div class="team-profile">
							<img src="/wp-content/uploads/2018/04/juliette-neu-1.jpg" alt="">
							<div class="details">
								<h2>Juliette Neu</h2>
								<h3>Chief Executive Officer</h3>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-6">
					<a href="/who-we-are/team/nicolas-hine/">
						<div class="team-profile">
							<img src="/wp-content/uploads/2018/04/nicolas-hine-1.jpg" alt="">
							<div class="details">
								<h2>Nicolas Hine</h2>
								<h3>Chief Financial Officer</h3>
							</div>
						</div>
					</a>
				</div>
				<!-- <div class="col-md-6">
					<div class="team-profile">
						<img src="/wp-content/uploads/2018/04/nicolas-hine.jpg" alt="">
						<h2>Matea Osojnik</h2>
						<h3>-</h3>
						<a href="/who-we-are/team/matea-osojnik/">Learn More ></a>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
