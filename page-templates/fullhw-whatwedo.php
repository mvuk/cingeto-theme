<?php
/**
 * Template Name: whatwedo
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu fiveMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>What we do</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/what-we-do/organizational-development/" class="msm-a">
				Organizational Development
			</a>
			<a href="/what-we-do/recruitment-talent/" class="msm-a">
				Recruitment & Talent
			</a>
			<a href="/what-we-do/global-growth/" class="msm-a">
				Global Growth
			</a>
			<a href="/what-we-do/coaching/" class="msm-a">
				Coaching
			</a>
			<a href="/what-we-do/ma-and-cultural-integration/" class="msm-a">
				M&A and Cultural Integration
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>What we do</h3>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/organizational-development/">
				Organizational Development
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/recruitment-talent/">
				Recruitment & Talent
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/global-growth/">
				Global Growth
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/coaching/">
				Coaching
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/ma-and-cultural-integration/">
				M&A and Cultural Integration
			</a>
		</div>
 	</div>
</div>

<div class="fullHW whatwedo full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="full-height-center">
				<div class="content">
					<h1>What we do</h1>
					<h2>Cingeto partners with organizations at all stages of development.</h2>
					<p>We are present in North America, Europe, East Asia and West Africa and have a demonstrated track record of navigating emerging market economies.</p>
				</div>
			</div>
			<!--  -->
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
