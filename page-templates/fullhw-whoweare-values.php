<?php
/**
 * Template Name: whoweare-values
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu" id="msmMenu" onclick="showMsm()">
			<span>Who we are</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/who-we-are/mission/" class="msm-a">
				Mission
			</a>
			<a href="/who-we-are/global/" class="msm-a">
				Global
			</a>
			<a href="/who-we-are/values/" class="msm-a active">
				Values
			</a>
			<a href="/who-we-are/team" class="msm-a">
				Team
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Who we are</h3>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/mission/">
				Mission
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/global/">
				Global
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/values/" class="active">
				Values
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/team/ ">
				Team
			</a>
		</div>
 	</div>
</div>

<div class="fullHW values full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content">
						<h1 class="min-title"><a href="/who-we-are/" class="linline-h1-link">Who we are</a>&gt; Values</h1>
						<!--  -->
						<div class="row">
							<div class="col-md-6">
								<div class="value">
									<h2>Sincerity</h2>
									<p>We stand by our word. And we never lose sight of what matters; people</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="value">
									<h2>Ingenuity</h2>
									<p>Our pathfinders solve complex organizational problems with finesse and creativity.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="value">
									<h2>Harmony</h2>
									<p>Harmony materializes when people share values, work well together and feel empowered as part of an organization that values and respects everyone.</p>
								</div>
							</div>
							<div class="col-md-6">
								<div class="value">
									<h2>Vigor</h2>
									<p>As our name suggests, we are strong leaders; full of resilience and energy but not without humility.</p>
								</div>
							</div>
						</div>
						<div class="outro-link">
							<a href="/what-we-do/" class="c-btn">What we do</a>
							<a href="/contact/" class="c-btn">Send us a message</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
