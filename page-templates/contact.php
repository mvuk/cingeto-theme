<?php
/**
 * Template Name: contact
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="fullHW contact full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="full-height-center">
				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<h2>Contact Us</h2>
						</div>
					</div>
					<div class="row contact-row">
						<div class="col-md-7">
							<h3 class="showMobile">Contact Form</h3>
							<div class="contact-form-wrapper">
								<?php echo do_shortcode( '[contact-form-7 id="88" title="Contact Cingeto"]' ); ?>
							</div>
						</div>
						<div class="col-md-5">
							<div class="business-details">
								<p>Cingeto<br>1629 K ST NW<br>Suite 300<br>Washington, DC<br>20006 USA</p>
								<div class="contact-details">
									<p><span>t :</span> +1 202-355-6413<br><span>e :</span> admin@cingeto.com</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--  -->
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
