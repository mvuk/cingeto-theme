<?php
/**
 * Template Name: whatwedo-ma-cultural-integration
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu fiveMsmMenu" id="msmMenu" onclick="showMsm()">
			<span>What we do</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/what-we-do/organizational-development/" class="msm-a">
				Organizational Development
			</a>
			<a href="/what-we-do/recruitment-talent/" class="msm-a">
				Recruitment & Talent
			</a>
			<a href="/what-we-do/global-growth/" class="msm-a">
				Global Growth
			</a>
			<a href="/what-we-do/coaching/" class="msm-a">
				Coaching
			</a>
			<a href="/what-we-do/ma-and-cultural-integration/" class="msm-a active">
				M&A and Cultural Integration
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>What we do</h3>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/organizational-development/">
				Organizational Development
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/recruitment-talent/">
				Recruitment & Talent
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/global-growth/">
				Global Growth
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/coaching/">
				Coaching
			</a>
		</div>
		<div class="sub-link">
			<a href="/what-we-do/ma-and-cultural-integration/" class="active">
				M&A and Cultural Integration
			</a>
		</div>
 	</div>
</div>

<div class="fullHW ma full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content">
						<h1 class="min-title"><a href="/what-we-do/" class="linline-h1-link">What we do</a>&gt; M&A and Cultural Integration</h1>
						<h2>Merger & Acquisition - Due Diligence</h2>
						<p>We give you critical information that is often overlooked to add insight to your decision-making process.</p>
						<p>Cingeto provides an in-depth report and analysis of a team's skill set, capabilities, and aspirations during the due-diligence period of an acquisition.</p>
						<p>With Cingeto's expertise, you know exactly who you would be welcoming into your organization.</p>

						<h2>Cultural Integration</h2>
						<p>Cultural alignment is critical to the healthy growth of an organization.</p>
						<p>Cingeto designs and facilitates custom action plans for cultural integration.</p>
						<p>We create a harmonious transition and lasting results for the future.</p>

						<div class="outro-link">
							<a href="/industries/" class="c-btn">Explore industries</a>
							<a href="/contact/" class="c-btn">Send us a message</a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
