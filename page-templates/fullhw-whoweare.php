<?php
/**
 * Template Name: who we are
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu" id="msmMenu" onclick="showMsm()">
			<span>Who we are</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/who-we-are/mission/" class="msm-a">
				Mission
			</a>
			<a href="/who-we-are/global/" class="msm-a">
				Global
			</a>
			<a href="/who-we-are/values/" class="msm-a">
				Values
			</a>
			<a href="/who-we-are/team" class="msm-a">
				Team
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Who we are</h3>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/mission/">
				Mission
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/global/">
				Global
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/values/">
				Values
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/team/">
				Team
			</a>
		</div>
 	</div>
</div>

<div class="fullHW whoweare full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="full-height-center">
				<div class="content">
					<h1>Who we are</h1>
					<h2>Cingeto is a global boutique firm providing innovative and profitable talent solutions to optimize your company's corporate performance</h2>
					<p>Cingeto means 'Warrior' in the ancient Gaulish language. The Gauls united vast territories (now Europe) and represented a uniquely progressive time in world history.</p>
					<p>Let Cingeto unite your company.</p>
					<p>We teach your staff to speak the same corporate language, thereby improving communication and performance.</p>
					<ul>
						<li>Aerospace</li>
						<li>Space</li>
						<li>Innovative Tech</li>
					</ul>
				</div>
			</div>
			<!--  -->
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
