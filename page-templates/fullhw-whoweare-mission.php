<?php
/**
 * Template Name: whoweare-mission
 *
 * Template for displaying a page just with the header and footer area and a "naked" content area in between.
 * Good for landingpages and other types of pages where you want to add a lot of custom markup.
 *
 * @package understrap
 */

get_header();

while ( have_posts() ) : the_post();
	get_template_part( 'loop-templates/content', 'empty' );
endwhile;
?>
<!-- PAGE CONTENT BEGIN -->

<div class="abs-right-menu image-bg hideDuringMenu" id="abs-menu">
	<!-- start mobile menu -->
	<div class="mobile-sub-menu hideDesktop" id="mobileSubMenu">
		<div class="msmMenu" id="msmMenu" onclick="showMsm()">
			<span>Who we are</span>
			<i class="far fa-chevron-down"></i>
		</div>
		<div class="msmItems" id="msmItems">
			<a href="/who-we-are/mission/" class="msm-a active">
				Mission
			</a>
			<a href="/who-we-are/global/" class="msm-a">
				Global
			</a>
			<a href="/who-we-are/values/" class="msm-a">
				Values
			</a>
			<a href="/who-we-are/team" class="msm-a">
				Team
			</a>
		</div>
	</div>
	<!-- end mobile menu -->
 	<div class="sub-links">
		<div class="sub-title">
			<h3>Who we are</h3>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/mission/" class="active">
				Mission
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/global/">
				Global
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/values/">
				Values
			</a>
		</div>
		<div class="sub-link">
			<a href="/who-we-are/team">
				Team
			</a>
		</div>
 	</div>
</div>

<div class="fullHW mission full-image">

	<div class="container">
		<div class="abs-container hideDuringMenu" id="abs-container">
			<!--  -->
			<div class="row std-scroll">
				<div class="col-md-12">
					<div class="content">
						<h1 class="min-title"><a href="/who-we-are/" class="linline-h1-link">Who we are</a>&gt; Mission</h1>
						<h2>Humans are rich, emotional, intelligent, moving, and complex. We place them at the heart of your business strategy to create a thriving constellation of talent.</p>
							<div class="outro-link">
								<a href="/what-we-do/" class="c-btn">What we do</a>
								<a href="/contact/" class="c-btn">Send us a message</a>
							</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>

</div>

<!-- PAGE CONTENT END -->
<?php
get_footer();
