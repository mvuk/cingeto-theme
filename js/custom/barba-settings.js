jQuery(document).ready(function($){

Barba.Prefetch.init();
Barba.Pjax.start();

Barba.Dispatcher.on('linkClicked', function(el) {
  lastElementClicked = el;
});

var FadeTransition = Barba.BaseTransition.extend({
  start: function() {
    /**
     * This function is automatically called as soon the Transition starts
     * this.newContainerLoading is a Promise for the loading of the new container
     * (Barba.js also comes with an handy Promise polyfill!)
     */
     var e = this;

    // As soon the loading is finished and the old page is faded out, let's fade the new page
    Promise
      .all([this.newContainerLoading, this.fadeOut()])
      .then(this.fadeIn.bind(this));
  },

  fadeOut: function() {
    /**
     * this.oldContainer is the HTMLElement of the old Container
     */

    // var absContainer = $("#abs-container");
    // console.log(absContainer);


    // return $(this.oldContainer).animate({ opacity: 0 }).promise();


    // TODO SWAP THIS OUT WITH MAKING THE OLD
    $(this.oldContainer).css({
      zIndex : -1,
      position : 'absolute'
    })
    $(this.oldContainer).find().css({

    })
    return $(this.oldContainer).find("#abs-container").animate({ opacity: 0 }, 300).promise();
  },

  fadeIn: function() {
    /**
     * this.newContainer is the HTMLElement of the new Container
     * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
     * Please note, newContainer is available just after newContainerLoading is resolved!
     */

    var _this = this;
    var $el = $(this.newContainer);
    var $ol = $(this.oldContainer);

    if (menuState) {
    // if the menu is open
      console.log(menuState);
      console.log("menu open");

      $("#overlayMenu").stop(true, true).delay(300).animate({
        opacity: 0
      }, 800, function() {
        // the rest of the actions
        $("#overlayMenu").css({
          zIndex : 0
        });

        menuState = false;
        console.log("menu should now be false:")
        console.log(menuState);

        menuSwitcher.className = "fal fa-bars";
        menuBtn.setAttribute("onclick", "show()");
        // rest of the program
        firstpartoftheprogram();
        restoftheprogram();


        // end callback
      });


      // make the menu close, full animation with a callback function. then the rest of the animations happen.

    // end menu open
    } else {
    // if the menu is closed
      console.log(menuState);
      console.log("menu closed");
      //  rest of the program
      firstpartoftheprogram();
      restoftheprogram();

    // end menu open
    }

    function firstpartoftheprogram() {
      // old container

      $ol.find("#abs-menu").css({
        visibility : 'hidden'
      })

      // this is already the new container
      $el.css({
        visibility : 'visible',
        position : 'absolute'
      })

      // use find to select just the bg area
      $el.find(".fullHW").css({
        visibility : 'visible',
        height: '0vh',
        padding: 0,
        bottom: 0,
        position: 'fixed'
      });

      $el.find("#abs-menu").css({
        visible : 'visible',
        opacity : 1
      });

      $el.find("#abs-container").css({
        opacity : 0
      });

      $ol.css({
        opacity: 1
      })

    }

    function restoftheprogram() {
      $ol.find("#abs-menu").animate({
        // height: '0%',
        // padding: 0,
        opacity: 0
        // overflow : 'hidden'
      }, 300);

      // $ol.animate({
      //   opacity: 0.5
      // }, 600, "swing")

      $el.find(".fullHW").animate({
        height: '100vh',
        padding: '100px 2rem',
        position: 'inherit',
        bottom: 'auto'
      }, 600, "swing");

      $el.find("#abs-container").delay(600).animate({
        opacity: 1
      }, 300, function() {
        /**
         * Do not forget to call .done() as soon your transition is finished!
         * .done() will automatically remove from the DOM the old Container
         */

         $el.find(".fullHW").css({
           position: 'relative'
         })

        _this.done();
      });
    }


  }
});

/**
 * Next step, you have to tell Barba to use the new Transition
 */

Barba.Pjax.getTransition = function() {
  /**
   * Here you can use your own logic!
   * For example you can use different Transition based on the current page or link...
   */

  return FadeTransition;
};

});
