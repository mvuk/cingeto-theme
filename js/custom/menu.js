var menuState = false;

var menuSwitcher = document.getElementById('menuSwitcher');

var menuBtn = document.getElementById('menuBtn');
var overlayMenu = document.getElementById('overlayMenu');

function defineMsm() {
  var msmMenu = document.getElementById('msmMenu');
  var msmItems = document.getElementById('msmItems');
}

function showMsm() {
  defineMsm();

  console.log("show msm");

  jQuery(document).ready(function($){
  //
  var maxHeightValue

    try {
      var isThreeMenu = document.getElementsByClassName("threeMsmMenu");
      console.log(isThreeMenu);
    } catch (e) {
      console.log(e)
    } finally {
    }

    try {
      var isFiveMenu = document.getElementsByClassName("fiveMsmMenu");
      console.log(isFiveMenu);
    } catch (e) {
      console.log(e)
    } finally {
    }

//

    if (isThreeMenu[0]) {
      console.log("there is three")
      maxHeightValue = "290px";
    } else if (isFiveMenu[0]) {
      console.log("there is five")
      maxHeightValue = "415px";
    } else {
      console.log("there is four")
      maxHeightValue = "350px";
    }

    $(".abs-right-menu").animate({
    // $("#abs-menu").animate({
      maxHeight: maxHeightValue,
    }, 400, "swing", function() {
      // callback

      $(".msmItems").css({
        display: 'flex',
      })
      $(".msmItems").animate({
        opacity: 1
      }, 200, "swing")
      //
    });
  //
  });

  // absmenu max height xxxpx

  // msm items ->
  // display: flex;
  // flex-direction: column;

  msmMenu.setAttribute("onclick", "hideMsm()");

}
function hideMsm() {
  defineMsm();

  console.log("hide msm");

  jQuery(document).ready(function($){
  //
    $("#msmItems").animate({
      opacity: 0,
    }, 200, "swing", function() {
      // callback

      $("#msmItems").css({
        display: 'none',
      })
      $("#abs-menu").animate({
        maxHeight: '90px'
      }, 400, "swing")
      //
    });
  //
  });

  msmMenu.setAttribute("onclick", "showMsm()");
}

// default state

function show() {
  // define the elements
  var absMenu = document.getElementById('abs-menu');
  var absContainer = document.getElementById('abs-container');

  // hide the content

  // try {
  //   absMenu.classList.add('abs-hide');
  // } catch (e) {
  //   console.log(e)
  // } finally {
  // }
  // try {
  //   absContainer.classList.add('abs-hide');
  // } catch (e) {
  //   console.log(e)
  // } finally {
  // }

  jQuery(document).ready(function($){
    $("#overlayMenu").css({
      zIndex: 0,
      opacity: 0
    })

  // $("#abs-menu").stop( true, true ).animate({
  //   opacity: 0
  // }, 800);
  // $("#abs-container").stop( true, true ).animate({
  //   opacity: 0
  // }, 800);
  $(".hideDuringMenu").stop( true, true ).animate({
    opacity: 0
  }, 800);
  $("#overlayMenu").css({
    zIndex: 100,
  })
  $("#overlayMenu").stop(true, true).delay(800).animate({
    opacity: 1
  }, 800);
  });


  // show the menu



  menuState = true;
  menuSwitcher.className = "fal fa-times";
  menuBtn.setAttribute("onclick", "hide()");
}

function hide() {
  // define the elements
  var absMenu = document.getElementById('abs-menu');
  var absContainer = document.getElementById('abs-container');

  // show the content

  // try {
  //     absMenu.classList.remove('abs-hide');
  // } catch (e) {
  //   console.log(e)
  // }
  // try {
  //   absContainer.classList.remove('abs-hide');
  // } catch (e) {
  //   console.log(e)
  // }

  jQuery(document).ready(function($){
    // $("#abs-menu").stop(true, true).delay(1200).animate({
    //   opacity: 1
    // }, 300);
    // $("#abs-container").stop(true, true).delay(1200).animate({
    //   opacity: 1
    // }, 300);
    $(".hideDuringMenu").stop(true, true).delay(1200).animate({
      opacity: 1
    }, 300);
    $("#overlayMenu").stop(true, true).delay(300).animate({
      opacity: 0,
    }, 800, function() {
      $("#overlayMenu").css({
        zIndex : 0
      });
    });


  });


  // hide the menu

  menuState = false;
  menuSwitcher.className = "fal fa-bars";
  menuBtn.setAttribute("onclick", "show()");
}

function menuToPage() {

  jQuery(document).ready(function($){

    $("#overlayMenu").stop(true, true).delay(300).animate({
      opacity: 0,
    }, 800, function() {
      $("#overlayMenu").css({
        zIndex : 0
      });
    });

  });

  // hide the menu

  // menuState = false;
  menuSwitcher.className = "fal fa-bars";
  menuBtn.setAttribute("onclick", "show()");

}

function fadeSelfAway() {

  jQuery(document).ready(function($){

    $("#abs-menu").stop(true, true).delay(300).animate({
      opacity: 0,
    }, 300);

  });

}
