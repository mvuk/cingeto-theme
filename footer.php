<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php get_sidebar( 'footerfull' ); ?>

<!-- <div class="wrapper" id="wrapper-footer">

	<div class="<?php echo esc_attr( $container ); ?>">

		<div class="row">

			<div class="col-md-12">

				<footer class="site-footer" id="colophon">

					<div class="site-info">

						<p>2018 Cingeto</p>

					</div>

				</footer>

			</div>

		</div>

	</div>

</div> -->

</div><!-- #page we need this extra closing tag here -->

</div><!-- .barba-container -->
</div> <!-- #barba-wrapper -->



<?php wp_footer(); ?>

</body>

</html>
