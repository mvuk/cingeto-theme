<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>

	<!-- prevent caching -->
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />

	<!-- <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet"> -->
	<link rel="stylesheet" href="https://use.typekit.net/rjp0xjn.css">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.1/TweenMax.min.js"></script>

	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-KwxQKNj2D0XKEW5O/Y6haRH39PE/xry8SAoLbpbCMraqlX7kUP6KHOnrlrtvuJLR" crossorigin="anonymous">

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118752147-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-118752147-1');
	</script>


</head>

<body <?php body_class(); ?>>

<div id="titleMenu">
	<div class="container">
		<div class="row justify-content-between headerRow">
			<div class="col-md-3">
				<a href="/" class="no-barba">
					<div class="logo" onclick="hide()">
						<img src="/wp-content/uploads/2018/04/cingeto_white.png" alt="Cingeto" class="cingeto-logo">
					</div>
				</a>
			</div>
			<div class="col-md-3" id="rightMenu">
				<div class="menuBtn" id="menuBtn" onclick="show()">
					<i id="menuSwitcher" class="fal fa-bars"></i>
					<span class="menuText">Menu</span>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="overlayMenu" style="z-index: 0; opacity: 0;">
	<div class="align-div">
		<!--  -->
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="hb-flex">
						<div class="business-details">
							<p>Cingeto<br>1629 K ST NW<br>Suite 300<br>Washington, DC<br>20006 USA</p>
							<div class="contact-details">
								<p><span>t :</span> +1 202-355-6413<br><span>e :</span> admin@cingeto.com</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="menu-pages-container" onclick="">
						<a href="/" class="no-barba">Home</a>
						<a href="/who-we-are/">Who we are</a>
						<a href="/what-we-do/">What we do</a>
						<a href="/industries/">Industries</a>
						<a href="/our-clients/">Our clients</a>
						<!-- <a href="/news/">News</a> -->
						<a href="/contact/">Contact</a>
					</div>
				</div>
			</div>
		</div>
		<!--  -->
	</div>
</div>

<div class="hfeed site" id="page">

<div id="barba-wrapper">
  <div class="barba-container">
	<!-- ******************* The Navbar Area ******************* -->
<!-- <div class="wrapper-fluid wrapper-navbar" id="wrapper-navbar" itemscope itemtype="http://schema.org/WebSite">
	<nav class="navbar navbar-expand-md navbar-dark" id="navbar">
		<div class="container">
			<div class="row justify-content-between headerRow">
				<div class="col-md-3">
					<a href="/">
						<div class="logo">
							Cigneto
						</div>
					</a>
				</div>
				<div class="col-md-3">
					<div class="menuBtn" id="menuBtn" onclick="show()">
						Menu <i class="fa fa-bars"></i>
					</div>
				</div>
			</div>
		</div>
	</nav>
</div> -->
